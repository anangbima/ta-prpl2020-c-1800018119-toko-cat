
<?php $this->load->view("pelanggan/layout/header.php") ?>
<title>Toko Cat Kita</title>
	<div class="jumbotron" style="height: 500px;">
	  <a class="btn btn-success btn-lg" href="#" role="button">Coming soon</a>
	</div>

	<div class="container mt-5">
		<div class="row">
		    <div class="col-3 border-right border-success ">
		    	<div class="col-12" style="height: 500px;">
		    		<div class="border-success border-bottom mb-3">
		    			<label>Search</label>
		    		</div>
		    		<form class="form-inline" action="<?php echo base_url().'pelanggan/cari' ?>" method="post">
				      	<input class="form-control form-control-sm w-100" type="search" placeholder="Search" aria-label="Search" name="cari">
				      	<div class="form-row mt-3">
				      		<div class="col-6">
				      			<input class="form-control mr-sm-2 form-control-sm w-100" name="minimal" placeholder="minimal">
				      		</div>
				      		<div class="col-6">
				      			<input class="form-control mr-sm-2 form-control-sm w-100" name="maksimal" placeholder="maksimal">
				      		</div>
				      	</div>
				      	
				    <!--  		<div class="form-check">
							  	<input class="form-check-input" type="checkbox" id="inlineCheckbox1" name="warna[]" value="putih">
							 	<label class="form-check-label" for="inlineCheckbox1">Putih</label>
							</div>
							<div class="form-check">
							  	<input class="form-check-input" type="checkbox" id="inlineCheckbox2" name="warna[]" value="merah">
							  	<label class="form-check-label" for="inlineCheckbox2">Merah</label>
							</div>
							<div class="form-check">
							  	<input class="form-check-input" type="checkbox" id="inlineCheckbox2" name="warna[]" value="kuning">
							  	<label class="form-check-label" for="inlineCheckbox2">Kuning</label>
							</div>
							<div class="form-check">
							  	<input class="form-check-input" type="checkbox" id="inlineCheckbox2" name="warna[]" value="hijau">
							  	<label class="form-check-label" for="inlineCheckbox2">Hijau</label>
							</div>
							<div class="form-check">
							  	<input class="form-check-input" type="checkbox" id="inlineCheckbox2" name="warna[]" value="biru">
							  	<label class="form-check-label" for="inlineCheckbox2">Biru</label>
							</div>
							<div class="form-check">
							  	<input class="form-check-input" type="checkbox" id="inlineCheckbox2" name="warna[]" value="hitam">
							  	<label class="form-check-label" for="inlineCheckbox2">Hitam</label>
							</div>
							<div class="form-check">
							  	<input class="form-check-input" type="checkbox" id="inlineCheckbox2" name="warna[]" value="pink">
							  	<label class="form-check-label" for="inlineCheckbox2">Pink</label>
							</div>-->
				      	
				      	<div class="mt-3">
				      		<button class="btn btn-outline-success my-2 my-sm-0 btn-sm" type="submit"><i class="fas fa-search"></i> Search</button>
				      	</div>
					</form>
		    	</div>
		    </div>
		   <div class="col-9">
		    	<!--<div class="col-12">-->
		    		<div class="border-bottom border-success d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center mb-3">
			    		<label>Produk Terbaru</label>
			    		<a href="<?php echo base_url().'pelanggan/produk' ?>" class="nav-item nav-link">view all</a>
			    	</div>
			    	
			    	<div class="row row-cols-1 ml-2">
			    		<div class="owl-carousel owl-theme" id="owl-demo">
			    			<?php  
								foreach($produk as $p):
							?>
				    			<div class="item">
				    				
							    		<div class="card border-success w-100" style="height: 255px;">
							    			<center><img src="<?php echo base_url().'/assets/upload/'.$p->foto; ?>" alt="gambar tidak tersedia" style="height: 100px; width: 100px;"></center>
											<div class="card-body">
											    <h6 class="card-title"><?=substr($p->merk, 0, 20); ?></h6>
											    <?php
													$color = explode(',', $p->warna);

													foreach ($color as $data) {
														if ($data == 'putih') {
															echo '<div class="box putih"></div>';
														} 
														else if ($data == 'merah') {
															echo '<div class="box merah"></div>';
														}
														else if ($data == 'kuning') {
															echo '<div class="box kuning"></div>';
														}	
														else if ($data == 'hijau') {
															echo '<div class="box hijau "></div>';
														}
														else if ($data == 'biru') {
															echo '<div class="box biru"></div>';
														}
														else if ($data == 'hitam') {
															echo '<div class="box hitam"></div>';
														}
														else if ($data == 'pink') {
															echo '<div class="box pink"></div>';
														}
													}
												?>
											</div>

											<div class="card-footer border-success" style="height: 45px;">
												<center><a href="<?php echo base_url().'pelanggan/beli/'.$p->id_produk; ?>" class="btn btn-sm p-1 btn-success w-100" style="margin-top: -5px; margin-left: -6px; margin-bottom: 15px;"> Rp. <?php echo $p->harga; ?></a></center>
											</div>
							    		</div>
							    	
						    	</div>				   
					    	<?php 
					    		endforeach;
					    	?>
					   	</div>
				   	</div>

				   	<div class="border-bottom border-success d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center mb-3">
			    		<label>Produk Terpopuler</label>
			    		<a href="<?php echo base_url().'pelanggan/produk' ?>" class="nav-item nav-link">view all</a>
			    	</div>
		    		
		    		<div class="row row-cols-1 ml-2">
			    		<div class="owl-carousel owl-theme" id="owl-demo">
			    			<?php  
								foreach($pro as $p):
							?>
				    			<div class="item">
				    				
							    		<div class="card border-success w-100" style="height: 255px;">
							    			<center><img src="<?php echo base_url().'/assets/upload/'.$p->foto; ?>" alt="gambar tidak tersedia" style="height: 100px; width: 100px;"></center>
											<div class="card-body">
											    <h6 class="card-title"><?=substr($p->merk, 0, 20); ?></h6>
											    <?php
													$color = explode(',', $p->warna);

													foreach ($color as $data) {
														if ($data == 'putih') {
															echo '<div class="box putih"></div>';
														} 
														else if ($data == 'merah') {
															echo '<div class="box merah"></div>';
														}
														else if ($data == 'kuning') {
															echo '<div class="box kuning"></div>';
														}	
														else if ($data == 'hijau') {
															echo '<div class="box hijau "></div>';
														}
														else if ($data == 'biru') {
															echo '<div class="box biru"></div>';
														}
														else if ($data == 'hitam') {
															echo '<div class="box hitam"></div>';
														}
														else if ($data == 'pink') {
															echo '<div class="box pink"></div>';
														}
													}
												?>
											</div>
											<div class="card-footer border-success" style="height: 45px;">
												<center><a href="<?php echo base_url().'pelanggan/beli/'.$p->id_produk; ?>" class="btn btn-sm p-1 btn-success w-100" style="margin-top: -5px; margin-left: -6px; margin-bottom: 15px;">Rp. <?php echo $p->harga; ?></a></center>
											</div>
							    		</div>
							    	
						    	</div>				   
					    	<?php 
					    		endforeach;
					    	?>
					   	</div>
				   	</div>
		  </div>
		</div>
	</div>

<?php $this->load->view("pelanggan/layout/footer.php") ?>
