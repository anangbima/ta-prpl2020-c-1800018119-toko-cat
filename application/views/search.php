<?php $this->load->view("pelanggan/layout/header.php") ?>
<title>
	Hasil Pencarian
</title>
	<div class="container mt-5">
		<div class="row"  style="margin-top: 85px;">
		    <div class="col-3 border-right border-success">
		    	<div class="col-12" style="height: 500px;">
		    		<label class="border-success border-bottom">Search</label>
		    		<form class="form-inline" action="<?php echo base_url().'pelanggan/cari' ?>" method="post">
				      	<input class="form-control form-control-sm w-100" type="search" placeholder="Search" aria-label="Search" name="cari">
				      	<div class="form-row mt-3">
				      		<div class="col-6">
				      			<input class="form-control mr-sm-2 form-control-sm w-100" name="minimal" placeholder="minimal">
				      		</div>
				      		<div class="col-6">
				      			<input class="form-control mr-sm-2 form-control-sm w-100" name="maksimal" placeholder="maksimal">
				      		</div>
				      	</div>
				      	<div class="mt-3">
				      		<button class="btn btn-outline-success my-2 my-sm-0 btn-sm" type="submit">Search</button>
				      	</div>
					</form>
		    	</div>
		    </div>
		   <div class="col-9">
		   			<div class="border-bottom border-success d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center mb-3">
			    		<label>Hasil Pencarian</label>
			    	</div>
		    	<!--<div class="col-12">-->
		    		<div class="row row-cols-1 row-cols-md-5 ml-1">
		    			<?php  
							foreach($produk as $p):
						?>
		    			<div class="col col mb-4">
				    		<div class="card border-success" style="width: 152px; height: 255px;">
				    			<center><img src="<?php echo base_url().'/assets/upload/'.$p->foto; ?>" alt="gambar tidak tersedia" style="height: 100px; width: 100px; margin-top: 5px;"></center>
								<div class="card-body">
								    <h6 class="card-title"><?=substr($p->merk, 0, 20); ?></h6>
								    <?php
													$color = explode(',', $p->warna);

													foreach ($color as $data) {
														if ($data == 'putih') {
															echo '<div class="box putih"></div>';
														} 
														else if ($data == 'merah') {
															echo '<div class="box merah"></div>';
														}
														else if ($data == 'kuning') {
															echo '<div class="box kuning"></div>';
														}	
														else if ($data == 'hijau') {
															echo '<div class="box hijau "></div>';
														}
														else if ($data == 'biru') {
															echo '<div class="box biru"></div>';
														}
														else if ($data == 'hitam') {
															echo '<div class="box hitam"></div>';
														}
														else if ($data == 'pink') {
															echo '<div class="box pink"></div>';
														}
													}
												?>
								</div>
								<div class="card-footer border-success" style="height: 45px;">
									<center><a href="<?php echo base_url().'pelanggan/beli/'.$p->id_produk; ?>" class="btn btn-sm p-1 btn-success w-100" style="margin-top: -5px; margin-left: -6px; margin-bottom: 15px;">Rp. <?php echo $p->harga; ?></a></center>
								</div>
				    		</div>
				    	</div>
				    	<?php 
				    		endforeach;
				    	?>
				   	</div> 
		    	<!--</div>-->
		  </div>
		</div>
	</div>

<?php $this->load->view("pelanggan/layout/footer.php") ?>