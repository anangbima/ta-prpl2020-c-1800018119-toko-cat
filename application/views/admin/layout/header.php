<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link rel="stylesheet" href="<?php echo base_url('assets/font/css/all.css') ?>">

	<link href="<?php echo base_url('assets/css/bootstrap.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/style.css') ?>" rel="stylesheet">
	<script type="text/javascript" href="<?php echo base_url('assets/js/bootstrap.js') ?>"></script>
	<style type="text/css">
		.warna{
			background-color: black !important;
		}
		.box{
			border-style: solid;
			border-width: 1px;
			width: 16px;
			height: 16px;
			border-color: black;
			margin-right: 5px;
			margin-top: 5px;
			float: left;
		}

		.putih{
			background-color: white !important;
		}

		.merah{
			background-color: red !important;
		}

		.kuning{
			background-color: yellow !important;
		}

		.hijau{
			background-color: green !important;
		}

		.biru{
			background-color: blue !important;
		}

		.hitam{
			background-color: black !important;
		}

		.pink{
			background-color: pink !important;
		}

		.bodyyy{
			background-image: url('assets/image/brickwall.png');
		}
	</style>
</head>
	<nav class="navbar navbar-expand-lg navbar-light shadow-sm border-bottom border-success">
		<div class="container">
			<a class="navbar-brand font-weight-bold" href="<?php echo base_url().'admin/index' ?>">Toko Cat AHHAY</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    		<span class="navbar-toggler-icon"></span>
    		</button>
			<div class="collapse navbar-collapse" id="navbarNav">
			    <ul class="navbar-nav mr-auto">
			      	<li class="nav-item">
			        	<a class="nav-link" href="<?php echo base_url().'admin/produk' ?>"><i class="fas fa-paint-roller"></i> Produk</a>
			      	</li>
			      	<li class="nav-item">
			        	<a class="nav-link" href="<?php echo base_url().'admin/pembeli' ?>"><i class="fas fa-user"></i> Pembeli</a>
			      	</li>
			      	<li class="nav-item">
			        	<a class="nav-link" href="<?php echo base_url().'admin/transaksi' ?>"><i class="fas fa-shopping-cart"></i> Transaksi</a>
			      	</li>
			      	<li class="nav-item">
			        	<a class="nav-link" href="#"><i class="fas fa-chart-line"></i>Laporan</a>
			      	</li>
			    </ul>
			    <a href="#" class="btn btn-outline-success btn-sm mr-3"><i class="fas fa-user-plus"></i></a> 
				<a href="<?php echo base_url().'admin/logout' ?>" class="btn btn-outline-danger btn-sm"><i class="fas fa-sign-out-alt"></i></a>
			</div>
		</div>
	</nav>
<body>
