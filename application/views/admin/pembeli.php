<?php $this->load->view("admin/layout/header.php") ?>
<title>
	Pembeli
</title>
<div class="container mt-4">
	<div class="border-bottom">
		<h3>PEMBELINYO</h3>
	</div>
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mt-4">
		<a href="#" class="btn btn-outline-primary btn-sm">lporan mingguan</a>
		<form class="form-inline my-2 my-lg-0" action="<?php echo base_url().'admin/pembeli' ?>" method="post">
	      <input class="form-control mr-sm-2 form-control-sm" type="search" placeholder="Search" aria-label="Search" name="cari">
	      <button class="btn btn-outline-success my-2 my-sm-0 btn-sm" type="submit"><i class="fas fa-search"></i></button>
	    </form>
	</div>
	<div class="table-responsive mt-2">	
		<table class="table table-bordered table-hover table-sm">
			<thead>
				<tr>
					<th scope="col">id pembeli</th>
					<th scope="col">nama</th>
					<th scope="col">jenis kelamin</th>
					<th scope="col">no telepon</th>
					<th scope="col">alamat</th>
				</tr>
			</thead>
			<tbody>
				<?php  
					foreach($pembeli as $p):
				?>
				<tr>
					<td><?php echo $p->id_pembeli; ?></td>
					<td><?php echo $p->nama_pembeli; ?></td>
					<td><?php echo $p->jenis_kelamin; ?></td>
					<td><?php echo $p->no_telepon; ?></td>
					<td><?php echo $p->alamat; ?></td>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>

<?php $this->load->view("admin/layout/footer.php") ?>