<?php $this->load->view("admin/layout/header.php") ?>
<title>
	Produk
</title>
<div class="container mt-4">
	<div class="border-bottom d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-4">
		<h3>PRODUK NICh</h3>
		<a type="button" href="<?php echo base_url().'admin/tambah_produk' ?>" class="btn btn-outline-primary btn-sm"><i class="fas fa-plus"></i> Produk</a>
	</div>
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2">
		<div></div>
		<form class="form-inline my-2 my-lg-0" action="<?php echo base_url().'admin/produk' ?>" method="post">
	      <input class="form-control mr-sm-2 form-control-sm" type="search" placeholder="Search" aria-label="Search" name="cari">
	      <button class="btn btn-outline-success my-2 my-sm-0 btn-sm" type="submit"><i class="fas fa-search"></i></button>
	    </form>
	</div>
	<div class="table-responsive mt-2">	
		<table class="table table-bordered table-hover table-sm">
			<thead>
				<tr>
					<th scope="col">id produk</th>
					<th scope="col">merk</th>
					<th scope="col">satuan</th>
					<th scope="col">warna</th>
					<th scope="col">harga</th>
					<th scope="col">detail</th>
					<th scope="col">stok</th>
					<th scope="col">fotonya</th>
					<th scope="col">aksi</th>
				</tr>
			</thead>
			<tbody>
				<?php  
					foreach($produk as $p):
				?>
				<tr>
					<td><?php echo $p->id_produk; ?></td>
					<td><?php echo $p->merk; ?></td>
					<td><?php echo $p->satuan; ?></td>
					<td>
						<?php
							$color = explode(',', $p->warna);

							foreach ($color as $data) {
								if ($data == 'putih') {
									echo '<div class="box putih"></div>';
								} 
								else if ($data == 'merah') {
									echo '<div class="box merah"></div>';
								}
								else if ($data == 'kuning') {
									echo '<div class="box kuning"></div>';
								}	
								else if ($data == 'hijau') {
									echo '<div class="box hijau "></div>';
								}
								else if ($data == 'biru') {
									echo '<div class="box biru"></div>';
								}
								else if ($data == 'hitam') {
									echo '<div class="box hitam"></div>';
								}
								else if ($data == 'pink') {
									echo '<div class="box pink"></div>';
								}
							}
						?>
					</td>
					<td>Rp.<?php echo $p->harga; ?></td>
					<td><?php echo $p->detail; ?></td>
					<td><?php echo $p->stok; ?></td>
					<td><img src="<?php echo base_url().'/assets/upload/'.$p->foto; ?>" width="100" alt="gambar tidak tersedia"></td>
					<td>
						<a href="<?php echo base_url().'admin/edit_produk/'.$p->id_produk; ?>" class="btn btn-outline-success btn-sm w-100"><i class="fas fa-edit"></i></a>
						<a href="<?php echo base_url().'admin/hapus_produk/'.$p->id_produk; ?>" class="btn btn-outline-danger btn-sm w-100 mt-2"><i class="fas fa-trash-alt"></i></a>
					</td>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>

<?php $this->load->view("admin/layout/footer.php") ?>