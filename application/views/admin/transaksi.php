<?php $this->load->view("admin/layout/header.php") ?>
<title>
	Transaksi
</title>
<div class="container mt-4">
	<div class="border-bottom">
		<h3>TRANSAKSI MEN</h3>
	</div>
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mt-4">
		<a href="#" class="btn btn-outline-primary btn-sm">lporan mingguan</a>
		<form class="form-inline my-2 my-lg-0" action="<?php echo base_url().'admin/transaksi' ?>" method="post">
	      <input class="form-control mr-sm-2 form-control-sm" type="search" placeholder="Search" aria-label="Search" name="cari">
	      <button class="btn btn-outline-success my-2 my-sm-0 btn-sm" type="submit"><i class="fas fa-search"></i></button>
	    </form>
	</div>
	<div class="table-responsive mt-2">	
		<table class="table table-bordered table-hover table-sm">
			<thead>
				<tr>
					<th scope="col">id transaksi</th>
					<th scope="col">id pembeli</th>
					<th scope="col">id produk</th>
					<th scope="col">tgl transaksi</th>
					<th scope="col">total pembayarn</th>
					<th scope="col">jumlah beli</th>
					<th scope="col">status</th>
				</tr>
			</thead>
			<tbody>
				<?php  
					foreach($transaksi as $p):
				?>
				<tr>
					<td><?php echo $p->id_transaksi; ?></td>
					<td><?php echo $p->id_pembeli; ?></td>
					<td><?php echo $p->id_produk; ?></td>
					<td><?php echo $p->tgl_transaksi; ?></td>
					<td><?php echo $p->total_pembayaran; ?></td>
					<td><?php echo $p->jumlah_beli; ?></td>
					<td>
						<?php 
							if ($p->status == "memesan") {
								echo "<div class='btn-success btn-sm'><center><i class='fas fa-shopping-basket'></i>  memesan</center></div>";
							}
							else if ($p->status == "dalam pengiriman"){
								echo "<div class='btn-success btn-sm'><center><i class='fas fa-truck'></i>  on the way</center></div>";
							}
							else if ($p->status == "terkirim"){
								echo "<div class='btn-success btn-sm'><center><i class='fas fa-check'></i>  terkirim</center></div>";
							}
						?>
					</td>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>

<?php $this->load->view("admin/layout/footer.php") ?>