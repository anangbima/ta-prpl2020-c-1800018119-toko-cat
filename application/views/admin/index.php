
<?php $this->load->view("admin/layout/header.php") ?>
<?php //$this->load->view("admin/layout/navbar.php") ?>
<title>
	Dashboard Cat Kita
</title>
<div class="container mt-4">
	<div class="page-header">
		<div class="border-bottom border-success">
			<h3>SELAMAT DATANG DI THE REAL DASHBOARD MY BRO</h3>
		</div>
		<div class="mt-3">
			<a href="<?php echo base_url().'admin/pesanan' ?>">Pesanan</a>
		</div>
		<div class="row">
			<div class="col-2">
				
			</div>
			<div class="col-4">
				<div class="card mb-3" style="max-width: 20rem;">
					<div class="card-header d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
						<h6>Pesanan Baru</h6>
						<a href="<?php echo base_url().'admin/pesanan' ?>" style="color: black;"><i class="fas fa-eye"></i></a>
					</div>
					<div class="card-body text-center" style="height: 150px;">
				   		<div class="display-3" style="font-weight: bold;"><?php echo $transaksi;?></div>
				    	<p class="card-text"></p>
				  	</div>
				</div>
			</div>
			<div class="col-4">
				<div class="card mb-3" style="max-width: 20rem;">
					<div class="card-header d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
						<h6>Otw</h6>
						<a href="<?php echo base_url().'admin/pesanan' ?>" style="color: black;"><i class="fas fa-eye"></i></a>
					</div>
					<div class="card-body text-center" style="height: 150px;">
				   		<div class="display-3" style="font-weight: bold;"><?php echo $transaksi2;?></div>
				    	<p class="card-text"></p>
				  	</div>
				</div>
			</div>
			<div class="col-2">
				
			</div>
		</div>
		<div class="row">
			<div class="col-3">
				<div class="card mb-3" style="max-width: 20rem;">
					<div class="card-header d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
						<h6>Berhasil Terkirim</h6>
						<a href="<?php echo base_url().'admin/transaksi' ?>" style="color: black;"><i class="fas fa-eye"></i></a>
					</div>
					<div class="card-body text-center" style="height: 150px;">
				   		<div class="display-3" style="font-weight: bold;"><?php echo $transaksi3;?></div>
				    	<p class="card-text"></p>
				  	</div>
				</div>
			</div>
			<div class="col-3">
				<div class="card mb-3" style="max-width: 20rem;">
					<div class="card-header d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
						<h6>Toltal Transaksi</h6>
						<a href="<?php echo base_url().'admin/transaksi' ?>" style="color: black;"><i class="fas fa-eye"></i></a>
					</div>
					<div class="card-body text-center" style="height: 150px;">
				   		<div class="display-3" style="font-weight: bold;"><?php echo $transaksi4;?></div>
				    	<p class="card-text"></p>
				  	</div>
				</div>
			</div>
			<div class="col-3">
				<div class="card mb-3" style="max-width: 20rem;">
					<div class="card-header d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
						<h6>Produk</h6>
						<a href="<?php echo base_url().'admin/produk' ?>" style="color: black;"><i class="fas fa-eye"></i></a>
					</div>
					<div class="card-body text-center" style="height: 150px;">
				   		<div class="display-3" style="font-weight: bold;"><?php echo $produk;?></div>
				    	<p class="card-text"></p>
				  	</div>
				</div>
			</div>
			<div class="col-3">
				<div class="card mb-3" style="max-width: 20rem;">
					<div class="card-header d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
						<h6>Pembeli</h6>
						<a href="<?php echo base_url().'admin/pembeli' ?>" style="color: black;"><i class="fas fa-eye"></i></a>
					</div>
					<div class="card-body text-center" style="height: 150px;">
				   		<div class="display-3" style="font-weight: bold;"><?php echo $pembeli;?></div>
				    	<p class="card-text"></p>
				  	</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php $this->load->view("admin/layout/footer.php") ?>
