<?php $this->load->view("admin/layout/header.php") ?>
<title>
	Tambah Produk
</title>
<div class="container mt-4">
	<div class="border-bottom">
		<h3>TAMBAH PRODUK COY</h3>
	</div>
	<form class="mt-4 font-weight-bold" action="<?php echo base_url().'admin/tambah_produk_act' ?>" method="post" enctype="multipart/form-data">
		<div class="form-group row">
		    	<input type="hidden" readonly class="form-control" id="formGroupExampleInput" name="id_produk" value="PR<?php echo sprintf("%04s", $id_produk) ?>">
		</div>
		<div class="form-group row">
		    <label class="col-sm-2 col-form-label">Merk</label>
		    <div class="col-sm-10">
		    	<input type="text" class="form-control" id="formGroupExampleInput2" placeholder="Masukkan Merk" name="merk">
		    </div>
		</div>
		<div class="form-group row">
		    <label class="col-sm-2 col-form-label">satuan</label>
		    <div class="col-sm-10">
		    	<input type="text" class="form-control" id="formGroupExampleInput2" placeholder="Masukkan Merk" name="satuan">
		    </div>
		</div>
		<div class="form-group row">
			<label class="col-sm-2 col-form-label">Warna</label>
			<div class="col-sm-10">
				<div class="form-check">
				  	<input class="form-check-input" type="checkbox" id="inlineCheckbox1" name="warna[]" value="putih">
				 	<label class="form-check-label" for="inlineCheckbox1">Putih</label>
				</div>
				<div class="form-check">
				  	<input class="form-check-input" type="checkbox" id="inlineCheckbox2" name="warna[]" value="merah">
				  	<label class="form-check-label" for="inlineCheckbox2">Merah</label>
				</div>
				<div class="form-check">
				  	<input class="form-check-input" type="checkbox" id="inlineCheckbox2" name="warna[]" value="kuning">
				  	<label class="form-check-label" for="inlineCheckbox2">Kuning</label>
				</div>
				<div class="form-check">
				  	<input class="form-check-input" type="checkbox" id="inlineCheckbox2" name="warna[]" value="hijau">
				  	<label class="form-check-label" for="inlineCheckbox2">Hijau</label>
				</div>
				<div class="form-check">
				  	<input class="form-check-input" type="checkbox" id="inlineCheckbox2" name="warna[]" value="biru">
				  	<label class="form-check-label" for="inlineCheckbox2">Biru</label>
				</div>
				<div class="form-check">
				  	<input class="form-check-input" type="checkbox" id="inlineCheckbox2" name="warna[]" value="hitam">
				  	<label class="form-check-label" for="inlineCheckbox2">Hitam</label>
				</div>
				<div class="form-check">
				  	<input class="form-check-input" type="checkbox" id="inlineCheckbox2" name="warna[]" value="pink">
				  	<label class="form-check-label" for="inlineCheckbox2">Pink</label>
				</div>
			</div>
		</div>
		<div class="form-group row">
		    <label class="col-sm-2 col-form-label">harga</label>
		    <div class="col-sm-10">
		    	<input type="text" class="form-control" id="formGroupExampleInput2" placeholder="Masukkan Merk" name="harga">
		    </div>
		</div>
		<div class="form-group row">
		    <label class="col-sm-2 col-form-label">stok</label>
		    <div class="col-sm-10">
		    	<input type="number" class="form-control" id="formGroupExampleInput2" placeholder="Masukkan stok" name="stok">
		    </div>
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Detail</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="formGroupExampleInput" name="detail" placeholder="Masukkan detail">
			</div>
		</div>
		<div class="form-group-row">
			<label class="col-sm-2 col-form-label">Foto</label>
				<input type="file" name="foto">
		</div>
		<div class="form-group-row">
			<input type="submit" value="simpan">
		</div>
	</form>
</div>

<?php $this->load->view("admin/layout/footer.php") ?>