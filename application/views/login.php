<!DOCTYPE html>
<html>
<head>
	<title>Login</title>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link href="<?php echo base_url('assets/css/bootstrap.css') ?>" rel="stylesheet">
	<script type="text/javascript" href="<?php echo base_url('assets/js/bootstrap.js') ?>"></script>
	<style type="text/css">
		.warna{
			background-color: black !important;
		}
	</style>
</head>
<body>
<center>
<div class="container">
	<div class="col-md-4 col-md-offset-4 border shadow" style="margin-top:150px; padding: 20px;">
			<h2>LOGIN ADMIN</h2>
	<!--	<?php
			if(isset($_GET['pesan'])){
				if($_GET['pesan'] == "gagal"){
					echo "<div class='alert alert-danger alert-danger'>";
					echo $this->session->flashdata('alert');
					echo "</div>";
				}else if($_GET['pesan'] == "logout"){
					if($this->session->flashdata())
					{
						echo "<div class='alert alert-danger alert-success'>";
						echo $this->session->flashdata('Anda Telah Logout');
						echo "</div>";
					}
					//echo "<div class='alert alert-success'>Anda telah logout.</div>";
				}else if($_GET['pesan'] == "belumlogin"){
					if($this->session->flashdata())
					{
						echo "<div class='alert alert-danger alert-primary'>";
						echo $this->session->flashdata('alert');
						echo "</div>";
					}
					//echo "<div class='alert alert-primary'>Silahkan login dulu.</div>";
				}
			}else{
				if($this->session->flashdata())
				{
					echo "<div class='alert alert-danger alert-message'>";
					echo $this->session->flashdata('alert');
					echo "</div>";
				}
			}
		?>
		<br/>-->
		<div class="outter-form-login">
			<form method="post" class="inner-login" action="<?php echo base_url().'pelanggan/autentikasi' ?>">
				<div class="form-group">
					<input type="text" name="username" placeholder="username" class="form-control">
					<?php echo form_error('username'); ?> 
				</div>

				<div class="form-group">
					<input type="password" name="password" placeholder="password" class="form-control">
					<?php echo form_error('password'); ?>
				</div>

				<div class="form-group">
					<center>
					<input type="submit" value="Login" class="btn btn-outline-primary mr-3">
					</center>
				</div>
			</form>
		</div>
	</div>
</div>
</center>
<script type="text/javascript">
	$('.alert-message').alert().delay(3000).slideUp('slow');
</script>


	<script src="<?php echo base_url('assets/js/bootstrap.bundle.min.js') ?>"></script>
</body>
</html>