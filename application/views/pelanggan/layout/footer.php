<script type="text/javascript">
		$('.owl-carousel').owlCarousel({
	    loop:true,
	    margin:10,
	    nav:true,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:3
	        },
	        1000:{
	            items:5
	        }
	    }
		})
	</script>
	
<script src="<?php echo base_url('assets/js/bootstrap.js') ?>"></script>

<div class="container-fluid border-top border-success" style="background-color: white; height: 300px;">  
	<div class="container">
		<div class="row p-3">
		 	<div class="col-4 border-right border-success" style="height: 250px;">
		 		<h5>Tentang kami</h5>
		 			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam velit metus, dictum sit amet neque a, varius rhoncus metus. Vestibulum et scelerisque nibh. Nunc fermentum massa ut ante pulvinar porttitor. Curabitur eu posuere augue. Nam rutrum eu eros at volutpat. Curabitur id metus suscipit, dictum massa vel,.</p>
		 	</div>
		 	<div class="col-4 border-right border-success" style="height: 250px;">
			 	<h5>Social Media</h5>
			 		<a href="#" style="color: black;"><i class="fab fa-facebook-f fa-lg mr-3"></i></a>
			 		<a href="#" style="color: black;"><i class="fab fa-instagram fa-lg mr-3"></i></a>
			 		<a href="#" style="color: black;"><i class="fab fa-twitter fa-lg"></i></a>
			</div>
			<div class="col-4">
				<h5>Kontak</h5>
					<i class="fas fa-map-marker-alt"></i> Suka Maju, Kongbeng, Kutai timur<br>
					<i class="fas fa-envelope"></i> anangbima11@gmail.com <br>
					<i class="fas fa-phone-alt"></i> 085292725585
			</div>
		</div>
	</div>
</div>
</div>
</body>
</html>