<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/style.css') ?>" rel="stylesheet">

	<script src="<?php echo base_url('assets/jquery-3.5.0.js') ?>"></script>

	<link rel="stylesheet" href="<?php echo base_url('assets/font/css/all.css') ?>">

	<link rel="stylesheet" href="<?php echo base_url('assets/owl-carousel/css/owl.carousel.css') ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/owl-carousel/css/owl.theme.default.css') ?>">
	<script src="<?php echo base_url('assets/owl-carousel/js/owl.carousel.js') ?>"></script>

	<style type="text/css">
	  .bakgron{
	  	background-image: url('assets/image/1043116.jpg');
	  	background-size: cover;
	  }

	  .box{
			border-style: solid;
			border-width: 1px;
			width: 16px;
			height: 16px;
			border-color: black;
			margin-right: 5px;
			margin-top: 5px;
			float: left;
		}

		.putih{
			background-color: white !important;
		}

		.merah{
			background-color: red !important;
		}

		.kuning{
			background-color: yellow !important;
		}

		.hijau{
			background-color: green !important;
		}

		.biru{
			background-color: blue !important;
		}

		.hitam{
			background-color: black !important;
		}

		.pink{
			background-color: pink !important;
		}

		.bodyyy{
			background-image: url('assets/image/brickwall.png') !important;
		}
	</style>
</head>
<body class="bg-light bodyyy">
	<nav class="navbar navbar-expand-lg navbar-light shadow putih fixed-top border-bottom border-success">
		<div class="container">
			<a class="navbar-brand font-weight-bold" href="<?php echo base_url().'pelanggan/index' ?>">Toko Cat AHHAY</a>
			<a href="<?php echo base_url().'pelanggan/login' ?>" class="btn btn-outline-success btn-sm"><i class="fas fa-sign-in-alt"></i> Login</a>
		</div>
	</nav>
