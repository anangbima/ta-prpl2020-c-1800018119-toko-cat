<?php $this->load->view("pelanggan/layout/header.php") ?>
<?php  
	foreach($produk as $p):
?>
	<div class="container mt-5">
		<div style="margin-top: 85px;">
			<div class="border-bottom border-success">
				<h3>SEBELUM BELI INPUT DATA DULU CUY</h3>
			</div>
			<form class="mt-4 font-weight-bold form-sm mb-5" action="<?php echo base_url().'pelanggan/tambah_pembeli_act' ?>" method="post" enctype="multipart/form-data">
				<div>
					<img src="<?php echo base_url().'/assets/upload/'.$p->foto; ?>" alt="gambar tidak tersedia" style="width: 250px; height: 250px;" class="mb-3">
				</div>
				<div class="form-group row">
		    		<label class="col-sm-2 col-form-label">Id Produk</label>
				    <div class="col-sm-10">
				    	<input type="text" readonly class="form-control" id="formGroupExampleInput" placeholder="Masukkan Id Produk" name="id_produk" value="<?php echo $p->id_produk; ?>">
				    </div>
				</div>
				<div class="form-group row">
		    		<label class="col-sm-2 col-form-label">Harga</label>
				    <div class="col-sm-10">
				    	<input type="text" readonly class="form-control" id="formGroupExampleInput" name="harga" value="<?php echo $p->harga; ?>">
				    </div>
				</div>
				<!--<div class="form-group row">
					<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Id Pembeli</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="formGroupExampleInput" name="id_pembeli">
					</div>
				</div>-->
				<div class="form-group row">
					<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Nama</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="formGroupExampleInput" name="nama" placeholder="Masukkan Nama">
					</div>
				</div>
				<div class="form-group row">
					<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Jenis Kelamin</label>
					<div class="col-sm-10 mt-2">
						<input type="radio" name="jenis_kelamin" value="laki - laki">Laki Laki
						<input type="radio" name="jenis_kelamin" value="perempuan">Perempuan
					</div>
				</div>
				<div class="form-group row">
					<label for="formGroupExampleInput" class="col-sm-2 col-form-label">No Hp</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="formGroupExampleInput" name="no_telepon" placeholder="Masukkan No Hp">
					</div>
				</div>
				<div class="form-group row">
					<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Alamat</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="formGroupExampleInput" name="alamat" placeholder="Masukkan alamat">
					</div>
				</div>
				<!--<div class="form-group row">
					<label for="formGroupExampleInput" class="col-sm-2 col-form-label">id Transaksi</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="formGroupExampleInput" name="id_transaksi" placeholder="Masukkan id_transaksi">
					</div>
				</div>-->
				<div class="form-group row">
					<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Jumlah Beli</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="formGroupExampleInput" name="jumlah_beli" value="<?php $jumlah ?>">
					</div>
				</div>
				<button name="tambah" class="btn btn-success w-25">BELi</button>
			</form>
		</div>
	</div>
<?php  
	endforeach;
?>
<?php $this->load->view("pelanggan/layout/header.php") ?>