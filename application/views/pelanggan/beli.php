<?php $this->load->view("pelanggan/layout/header.php") ?>
<?php  
	foreach($produk as $p):
?>
	<title>
		<?php echo $p->merk; ?>
	</title>
		<div class="container mt-5">
			<div class="row"  style="margin-top: 85px;">
				<div class="col-9">
					<div class="card w-100">
						<div class="card-body border border-success">
							<div class="row">
								<div class="col-3 ml-2 mr-4">
									<img src="<?php echo base_url().'/assets/upload/'.$p->foto; ?>" alt="gambar tidak tersedia" style="width: 200px; height: 200px;">
								</div>
								<div class="col-8">
									<div class="border-bottom border-success">
										<h3><?php echo $p->merk; ?></h3>
									</div>
									<div class="card-text mt-2">
										<p>Rp. <?php echo $p->harga; ?></p>
									</div>
									<div class="card-text mt-2">
										<p>Stok : <?php echo $p->stok; ?></p>
									</div>
									<div class="card-text mt-2">
										<div>
											<?php
												$color = explode(',', $p->warna);

												foreach ($color as $data) {
													if ($data == 'putih') {
														echo '<div class="box putih"></div>';
													} 
													else if ($data == 'merah') {
														echo '<div class="box merah"></div>';
													}
													else if ($data == 'kuning') {
														echo '<div class="box kuning"></div>';
													}	
													else if ($data == 'hijau') {
														echo '<div class="box hijau "></div>';
													}
													else if ($data == 'biru') {
														echo '<div class="box biru"></div>';
													}
													else if ($data == 'hitam') {
														echo '<div class="box hitam"></div>';
													}
													else if ($data == 'pink') {
														echo '<div class="box pink"></div>';
													}
												}
											?>
										</div>
									</div>
									<!--<div>
										<form method="get" action="<?php echo base_url().'pelanggan/beli_temp/'.$p->id_produk; ?>">
											<input type="number" name="jumlah">
									</div>-->
									<div class="card-text mt-5">
										<h6>Detail</h6>
										<p><?php echo $p->detail; ?></p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="border-bottom border-success d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center mb-3">
					    <label>Produk Lainnya</label>
						<a href="<?php echo base_url().'pelanggan/produk' ?>" class="nav-item nav-link">view all</a>
			    	</div>
					    	
					   	<div class="row row-cols-1 ml-2 mr-1">
					   		<div class="owl-carousel owl-theme" id="owl-demo">
					   			<?php  
									foreach($produk2 as $p2):
								?>
					    			<div class="item">

								    		<div class="card border-success" style=" width : 155px; height: 255px;">
								    			<center><img src="<?php echo base_url().'/assets/upload/'.$p2->foto; ?>" alt="gambar tidak tersedia" style="height: 100px; width: 100px;"></center>
												<div class="card-body">
												    <h6 class="card-title"><?=substr($p2->merk, 0, 20); ?></h6>
												    <?php
														$color2 = explode(',', $p2->warna);

														foreach ($color2 as $data2) {
															if ($data2 == 'putih') {
																echo '<div class="box putih"></div>';
															} 
															else if ($data2 == 'merah') {
																echo '<div class="box merah"></div>';
															}
															else if ($data2 == 'kuning') {
																echo '<div class="box kuning"></div>';
															}	
															else if ($data2 == 'hijau') {
																echo '<div class="box hijau "></div>';
															}
															else if ($data2 == 'biru') {
																echo '<div class="box biru"></div>';
															}
															else if ($data2 == 'hitam') {
																echo '<div class="box hitam"></div>';
															}
															else if ($data2 == 'pink') {
																echo '<div class="box pink"></div>';
															}
														}
													?>
												</div>

												<div class="card-footer border-success" style="height: 45px;">
													<center><a href="<?php echo base_url().'pelanggan/beli/'.$p2->id_produk; ?>" class="btn btn-sm p-1 btn-success w-100" style="margin-top: -5px; margin-left: -6px; margin-bottom: 15px;"> Rp. <?php echo $p2->harga; ?></a></center>
												</div>
								    		</div>
									    	
								    </div>				   
							    <?php 
							   		endforeach;
							   	?>
						 	</div>
					   	</div>
				</div>
				<div class="col-3"">
					<div class="card w-100 border border-success">
						<div class="card-header border-bottom border-success">
							<div>isi Data</div>							
						</div>
						<div class="card-body">
							<form class="mt-1 form-sm mb-5" action="<?php echo base_url().'pelanggan/tambah_pembeli_act' ?>" method="post" enctype="multipart/form-data">
								<input type="hidden" readonly class="form-control" id="formGroupExampleInput" placeholder="Masukkan Id Produk" name="id_produk" value="<?php echo $p->id_produk; ?>">

								<input type="hidden" readonly class="form-control" id="formGroupExampleInput" name="harga" value="<?php echo $p->harga; ?>">
								<div class="form-group">
									<div>
										<input type="text" class="form-control form-control-sm" id="formGroupExampleInput" name="nama" placeholder="Nama">
									</div>
								</div>
								<div class="form-group">
									<div>
										<input type="radio" name="jenis_kelamin" value="laki - laki">Laki Laki
										<input type="radio" name="jenis_kelamin" value="perempuan">Perempuan
									</div>
								</div>
								<div class="form-group">
									<div>
										<input type="text" class="form-control form-control-sm" id="formGroupExampleInput" name="no_telepon" placeholder="No Hp" required="">
									</div>
								</div>
								<div class="form-group">
									<div>
										<input type="text" class="form-control form-control-sm" id="formGroupExampleInput" name="alamat" placeholder="alamat" required="">
									</div>
								</div>
								<div class="form-group">
									<div>
										<?php 
											$color3 = explode(',', $p->warna);

											foreach ($color3 as $data4) {
												if ($data4 == 'putih') {
													echo "<div class='form-check'>";
													echo "<input class='form-check-input' type='radio' id='inlineCheckbox1' name='warna' value='putih'>";
													echo '<label class="form-check-label" for="inlineCheckbox1"><div class="box putih"></div></label>';
													echo "</div>";
												} 
												else if ($data4 == 'merah') {
													echo "<div class='form-check'>";
													echo "<input class='form-check-input' type='radio' id='inlineCheckbox1' name='warna' value='merah'>";
													echo '<label class="form-check-label" for="inlineCheckbox1"><div class="box merah"></div></label>';
													echo "</div>";
												}
												else if ($data4 == 'kuning') {
													echo "<div class='form-check'>";
													echo "<input class='form-check-input' type='radio' id='inlineCheckbox1' name='warna' value='kuning'>";
													echo '<label class="form-check-label" for="inlineCheckbox1"><div class="box kuning"></div></label>';
													echo "</div>";
												}	
												else if ($data4 == 'hijau') {
													echo "<div class='form-check'>";
													echo "<input class='form-check-input' type='radio' id='inlineCheckbox1' name='warna' value='hijau'>";
													echo '<label class="form-check-label" for="inlineCheckbox1"><div class="box hijau"></div></label>';
													echo "</div>";
												}
												else if ($data4 == 'biru') {
													echo "<div class='form-check'>";
													echo "<input class='form-check-input' type='radio' id='inlineCheckbox1' name='warna' value='biru'>";
													echo '<label class="form-check-label" for="inlineCheckbox1"><div class="box biru"></div></label>';
													echo "</div>";
												}
												else if ($data4 == 'hitam') {
													echo "<div class='form-check'>";
													echo "<input class='form-check-input' type='radio' id='inlineCheckbox1' name='warna' value='hitam'>";
													echo '<label class="form-check-label" for="inlineCheckbox1"><div class="box hitam"></div></label>';
													echo "</div>";
												}
												else if ($data4 == 'pink') {
													echo "<div class='form-check'>";
													echo "<input class='form-check-input' type='radio' id='inlineCheckbox1' name='warna' value='pink'>";
													echo '<label class="form-check-label" for="inlineCheckbox1"><div class="box pink"></div></label>';
													echo "</div>";
												}
											}
										?>
									</div>
								</div>
								<div class="form-group">
									<div>
										<input type="number" class="form-control form-control-sm" id="formGroupExampleInput" name="jumlah_beli" placeholder="jumlah beli" value="<?php $jumlah ?>">
									</div>
								</div>
								<div class="form-group">
									<div>
										<!--<input type="text" class="form-control form-control-sm" id="formGroupExampleInput" name="metode" placeholder="metode pengiriman">-->
										<select class="form-control form-control-sm" name="metode">
											<option value="COD">COD</option>
											<option value="POS">POS</option>
										</select>
									</div>
								</div>
								<button name="tambah" class="btn btn-sm btn-success w-100">BELI</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>

<?php  
	endforeach;
?>

<?php $this->load->view("pelanggan/layout/footer.php") ?>