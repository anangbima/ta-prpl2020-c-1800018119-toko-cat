<?php  
defined('BASEPATH') or exit ('NO Direct Script Access Allowed');

/**
 * 
 */
class Admin extends CI_Controller
{
	
	//buat login?
	function __construct(){
		parent::__construct();
		// cek login
		if($this->session->userdata('status') != "login"){
			$alert=$this->session->set_flashdata('alert', 'Silahkan Login Dahulu');
			redirect(base_url());
		}
	}

	
	function index(){
		$data['transaksi'] = $this->db->query('SELECT * FROM transaksi where status ="memesan"')->num_rows();
		$data['transaksi2'] = $this->db->query('SELECT * FROM transaksi where status ="dalam pengiriman"')->num_rows();
		$data['transaksi3'] = $this->db->query('SELECT * FROM transaksi where status ="terkirim"')->num_rows();
		$data['produk'] = $this->db->query('SELECT * FROM produk')->num_rows();
		$data['pembeli'] = $this->db->query('SELECT * FROM pembeli')->num_rows();
		$data['transaksi4'] = $this->db->query('SELECT * FROM transaksi')->num_rows();
		$this->load->view('admin/index', $data);

	}

	function logout(){
		$this->session->sess_destroy();
		redirect(base_url().'pelanggan');
	}


	function pesanan(){
		$data['transaksi'] = $this->db->query('SELECT transaksi.id_transaksi, transaksi.total_pembayaran, transaksi.jumlah_beli, transaksi.status, transaksi.id_pembeli, transaksi.tgl_transaksi, pembeli.nama_pembeli, pembeli.no_telepon, pembeli.alamat, transaksi.id_produk, produk.merk, produk.harga, produk.foto FROM transaksi join pembeli on transaksi.id_pembeli = pembeli.id_pembeli join produk on transaksi.id_produk = produk.id_produk where transaksi.status = "memesan" or transaksi.status = "dalam pengiriman" order by transaksi.tgl_transaksi DESC')->result();
		$this->load->view('admin/pesanan', $data);
	}

	function pemesan($id){
		$where = array('id_transaksi' => $id);
		$data['transaksi'] = $this->db->query("SELECT transaksi.id_transaksi, transaksi.total_pembayaran, transaksi.jumlah_beli, transaksi.warna, transaksi.status, transaksi.id_pembeli, transaksi.tgl_transaksi, transaksi.metode, pembeli.nama_pembeli, pembeli.no_telepon, pembeli.alamat, pembeli.jenis_kelamin, transaksi.id_produk, produk.merk, produk.harga, produk.stok, produk.foto FROM transaksi join pembeli on transaksi.id_pembeli = pembeli.id_pembeli join produk on transaksi.id_produk = produk.id_produk where transaksi.id_transaksi = '$id'")->result();
		$this->load->view('admin/pemesan', $data);
	}

	function produk(){
		$key = $this->input->post('cari');

		if (null !== $key) {
			$data['produk'] = $this->model_cat->cari_produk('produk', $key);
			$this->load->view('admin/produk', $data);
		}
		else{
			$data['produk'] = $this->model_cat->get_data('produk')->result();
			$this->load->view('admin/produk', $data);
		}

	}

	function tambah_produk(){
		//$data['produk'] =$this->model_cat->get_data('produk')->result();

		

		$dariDB = $this->model_cat->kode_produk();
		$nourut = substr($dariDB, 2, 4);
		$kodejadi = $nourut + 1;
		$data = array('id_produk' => $kodejadi);

		//$data['produk'] = $this->db->query('SELECT MAX(id_produk) from produk');


		$this->load->view('admin/tambah_produk', $data);
	}

	function tambah_produk_act(){
		$id_produk = $this->input->post('id_produk');
		$merk = $this->input->post('merk');
		$satuan = $this->input->post('satuan');
		$harga = $this->input->post('harga');
		$detail = $this->input->post('detail');
		$warna = $this->input->post('warna');
		$stok = $this->input->post('stok');
		$this->form_validation->set_rules('id_produk','Id Produk','required');
		/*$this->form_validation->set_rules('merk','Merk','required');
		$this->form_validation->set_rules('satuan','Satuan','required');
		$this->form_validation->set_rules('harga','Harga','required');
		$this->form_validation->set_rules('detail','Detail','required');
		$this->form_validation->set_rules('warna','Warna','required');*/

		if ($this->form_validation->run() != FALSE) {
			//upload gambar
			$config['upload_path'] = './assets/upload/';
			$config['allowed_types'] = 'jpg|png|jpeg|webm';
			$config['max_size'] = '2048';
			$config['file_name'] = 'foto'.time();

			$this->load->library('upload', $config);

			if ($this->upload->do_upload('foto')) {
				$gambar=$this->upload->data();

				$color = implode(',', $warna);

				$data = array(
					'id_produk' => $id_produk,
					'merk' => $merk,
					'satuan' => $satuan,
					'warna' => $color,
					'harga' => $harga,
					'detail' => $detail,
					'stok' => $stok,
					'foto' => $gambar['file_name'],
				);

				$this->model_cat->insert_data($data, 'produk');
				redirect(base_url().'admin/produk');
			}
			else{
				$this->session->set_flashdata('alert', 'Anda Belum Memilih Foto Kamar');
			}
		}
		else{
			echo "salah";
		}
	}

	function edit_produk($id){
		$where = array('id_produk' => $id);
		$data['produk'] = $this->db->query("select * from produk where produk.id_produk='$id'")->result();
		//$data['produk'] = $this->model_cat->get_data("produk")->result();
		$this->load->view('admin/edit_produk', $data);
	}

	function update_produk(){
		$id_produk = $this->input->post('id_produk');
		$merk = $this->input->post('merk');
		$satuan = $this->input->post('satuan');
		$harga = $this->input->post('harga');
		$detail = $this->input->post('detail');
		$warna = $this->input->post('warna');
		$stok = $this->input->post('stok');
	
			$config['upload_path'] = './assets/upload/';
			$config['allowed_types'] = 'jpg|png|jpeg|webp';
			$config['max_size'] = '2048';
			$config['file_name'] = 'foto'.time();

			$this->load->library('upload', $config);

			$where = array('id_produk' => $id_produk);

			$color = implode(',', $warna);

			/*$data = array(
				'merk' => $merk,
				'satuan' => $satuan,
				'warna' => $color,
				'harga' => $harga,
				'detail' => $detail,
				'stok' => $stok,
				'foto' => $image['file_name'],
			);*/

			//versi baru

			if ($this->upload->do_upload('fotocat')) {
				$image = $this->upload->data();
				$data['foto'] = $image['file_name'];

				$data = array(
					'merk' => $merk,
					'satuan' => $satuan,
					'warna' => $color,
					'harga' => $harga,
					'detail' => $detail,
					'stok' => $stok,
					'foto' => $image['file_name'],
				);

				$this->model_cat->update_data($where, $data,'produk');
				redirect(base_url().'admin/produk');
			}else{
				$data = array(
					'merk' => $merk,
					'satuan' => $satuan,
					'warna' => $color,
					'harga' => $harga,
					'detail' => $detail,
					'stok' => $stok,
				);

				$this->model_cat->update_data($where, $data,'produk');
				redirect(base_url().'admin/produk');
			}

				/*if ($this->input->post('old_pict') != null) {
					$image = $this->input->post('old_pict');
					$data['foto'] = $image['file_name'];

				  	$this->model_cat->update_data($where, $data,'produk');
				}

				if($this->upload->do_upload('fotocat')){
				  	$image = $this->upload->data();
				  	unlink('assets/upload/'.$this->input->post('old_pict', TRUE));
			      	$data['foto'] = $image['file_name'];

				  	$this->model_cat->update_data($where, $data,'produk');
				}*/

			//$this->model_cat->update_data($where, $data,'produk');
			//redirect(base_url().'admin/produk');
	}

	function update_transaksi(){
		$id_transaksi = $this->input->post('id_transaksi');
		$id_produk = $this->input->post('id_produk');
		$id_pembeli = $this->input->post('id_pembeli');
		$tgl_transaksi = $this->input->post('tgl_transaksi');
		$total_pembayaran = $this->input->post('total_pembayaran');
		$jumlah_beli = $this->input->post('jumlah_beli');
		$status = $this->input->post('status');
		$metode = $this->input->post('metode');
		$stok = $this->input->post('stok');

		$where = array('id_transaksi' => $id_transaksi);

		$data = array(
			'id_produk' => $id_produk,
			'id_pembeli' => $id_pembeli,
			'tgl_transaksi' => $tgl_transaksi,
			'total_pembayaran' => $total_pembayaran,
			'jumlah_beli' => $jumlah_beli,
			'status' => $status, 
			'metode' => $metode,
		);

		if ($status == 'dalam pengiriman') {
			$stok_new = $stok - $jumlah_beli;

			$data2 = $this->db->query("UPDATE produk SET stok = '$stok_new' WHERE id_produk = '$id_produk'");

			$this->model_cat->update_data($where, $data,'transaksi');
			redirect(base_url().'admin/pesanan');
		}
		else{
			$this->model_cat->update_data($where, $data,'transaksi');
			redirect(base_url().'admin/pesanan');
		}

		//$this->model_cat->update_data($where, $data,'transaksi');
		//redirect(base_url().'admin/pesanan');
	}

	function hapus_produk($id){
		$where = array('id_produk' => $id);
		$this->model_cat->delete_data($where, 'produk');
		redirect(base_url().'admin/produk');
	}

	function pembeli(){
		$key = $this->input->post('cari');

		if (null !== $key) {
			$data['pembeli'] = $this->model_cat->cari_pembeli('pembeli', $key);
			$this->load->view('admin/pembeli', $data);
		}
		else{
			$data['pembeli'] = $this->model_cat->get_data('pembeli')->result();
			$this->load->view('admin/pembeli', $data);
		}
	}

	function transaksi(){
		$key = $this->input->post('cari');

		if (null !== $key) {
			$data['transaksi'] = $this->model_cat->cari_transaksi('transaksi', $key);
			$this->load->view('admin/transaksi', $data);
		}
		else{
			$data['transaksi'] = $this->model_cat->get_data('transaksi')->result();
			$this->load->view('admin/transaksi', $data);
		}
	}

	/*function cari(){
		if (isset($this->input->get('cari'))) {
			$key = $this->input->get('cari');
		}
	}*/

}

?>