<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelanggan extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct(){
		parent::__construct();
	}

	public function index()
	{
		//$data['produk'] = $this->model_cat->get_data('produk')->result();
		$data1['produk'] = $this->db->query("SELECT * FROM produk order by id_produk DESC LIMIT 10")->result();
		$data2['pro']  = $this->db->query("SELECT * FROM produk order by stok ASC LIMIT 10")->result();
		//$data['pro'] = $this->model_cat->populer();
		$this->load->view('index', $data1 + $data2);
	}

	function produk(){
		$data['produk'] = $this->db->query("SELECT * FROM produk order by id_produk DESC")->result();
		$this->load->view('pelanggan/produk', $data);
	}

	function cari(){
		$key = $this->input->post('cari');
		$min = $this->input->post('minimal');
		$max = $this->input->post('maksimal');
		//$warna = $this->input->post('warna');

		if (null !== $key AND null !== $min && $max){
			//$color = implode(',', $warna);
			$data['produk'] = $this->model_cat->cari('produk', $key, $min, $max);
			$this->load->view('search', $data);
		}else{
			$min = '0';
			$max = '99999999999999';

			$color = implode(',', $warna);
			$data['produk'] = $this->model_cat->cari('produk', $key, $min, $max);
			$this->load->view('search', $data);
		}

		/*if (null !== $key && $min == nulll && $max == null && $warna == null) {
			$min = '0';
			$max = '99999999999999';
			$color = 'unknown';

			$data['produk'] = $this->model_cat->cari('produk', $key, $min, $max, $color);
			$this->load->view('search', $data);
		}
		else if (null !== $min && null !== $max && $key == null && $warna == null) {
			$color = null; 
			$data['produk'] = $this->model_cat->cari('produk', $key, $min, $max, $color);
			$this->load->view('search', $data);
		}else if (null !== $warna && $min == nulll && $max == null && $key == null) {
			$color = implode(',', $warna);
			$data['produk'] = $this->model_cat->cari('produk', $key, $min, $max, $color);
			$this->load->view('search', $data);
		}*/
	}

	function login(){
		$this->load->view('login');
	}

	function beli($id){
		//$where = array('id_produk');
		//$data['produk'] = $this->db->beli($where);
		$where = array('id_produk' => $id);
		$data1['produk'] = $this->db->query("select * from produk where produk.id_produk='$id'")->result();
		$data2['produk2'] = $this->db->query("SELECT * FROM produk LIMIT 10")->result();
		$this->load->view('pelanggan/beli', $data1 + $data2);


	}

	function beli_temp($id){
		$jumlah = $this->input->post('jumlah');


	
		//if ($jumlah != null) {
			redirect(base_url().'pelanggan/membeli/'.$id.'/'.$jumlah.'/');
		//}
		//else{
		//	echo "masi salah";
		//}
	}

	function membeli($id){
		$where = array('id_produk' => $id);
		$data['produk'] = $this->db->query("select * from produk where produk.id_produk='$id'")->result();

		$this->load->view('pelanggan/membeli', $data); 
	}

	function tambah_pembeli_act(){
		$id_produk = $this->input->post('id_produk');
		$harga = $this->input->post('harga');
		//$id_pembeli = $this->input->post('id_pembeli');
		$nama = $this->input->post('nama');
		$jenis_kelamin = $this->input->post('jenis_kelamin');
		$no_hp = $this->input->post('no_telepon');
		$alamat = $this->input->post('alamat');
		//$id_transaksi = $this->input->post('id_transaksi');
		$jumlah_beli = $this->input->post('jumlah_beli');
		$warna = $this->input->post('warna');
		$total_pembayaran = $jumlah_beli * $harga;
		$metode = $this->input->post('metode');
		$this->form_validation->set_rules('id_produk','Id Produk','required');
		//$this->form_validation->set_rules('id_pembeli','Id Pembeli','required');
		$this->form_validation->set_rules('nama','nama','required');
		$this->form_validation->set_rules('jenis_kelamin','jenis_kelamin','required');
		$this->form_validation->set_rules('no_telepon
			','no_telepon','required');
		//$this->form_validation->set_rules('id_transaksi','Id id_transaksi','required');
		$this->form_validation->set_rules('jumlah_beli','jumlah_beli','required');

		$dariDB = $this->model_cat->kode_pembeli();
		$nourut = substr($dariDB, 2, 4);
		$kodejadi = $nourut + 1; 

		$idbaru = "CS".sprintf("%04s",$kodejadi);
		//$data2 = array('id_pembeli' => $kodejadi);

		$data = array(
			'id_pembeli' => $idbaru,
			'nama_pembeli' => $nama,
			'no_telepon' => $no_hp,
			'jenis_kelamin' => $jenis_kelamin,
			'alamat' => $alamat,
		);

		$this->model_cat->insert_data($data, 'pembeli');

		if ($data != null) {
			$dariDB2 = $this->model_cat->kode_transaksi();
			$nourut2 = substr($dariDB2, 2, 4);
			$kodejadi2 = $nourut2 + 1;

			$idbaru2 = "TR".sprintf("%04s",$kodejadi2);

			$data2 = array(
				'id_transaksi' => $idbaru2,
				'id_pembeli' => $idbaru,
				'id_produk' => $id_produk,
				'total_pembayaran' => $total_pembayaran,
				'jumlah_beli' => $jumlah_beli,
				'warna' => $warna,
				'metode' => $metode,
				'status' => 'memesan',
			);

			$this->model_cat->insert_data($data2, 'transaksi');

			if ($data2 != null) {
				if ($metode == 'COD') {
					redirect(base_url().'pelanggan/finish_cod');
				}
				else if ($metode == 'POS') {
					redirect(base_url().'pelanggan/finish_pos');
				}
			}
		}
	}

	function finish_cod(){
		$this->load->view('pelanggan/finish_cod');
	}

	function finish_pos(){
		$this->load->view('pelanggan/finish_pos');
	}

	function autentikasi(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$this->form_validation->set_rules('username','Username','trim|required');
		$this->form_validation->set_rules('password','Password','trim|required');

		if ($this->form_validation->run() != FALSE) {
			$where = array('username' => $username, 'password' => md5($password) );

			$data = $this->model_cat->edit_data($where, 'admin');
			$d = $this->model_cat->edit_data($where, 'admin')->row();
			$cek = $data->num_rows();

			if ($cek > 0) {
				$session = array('nama' => $d->namma_admin, 'status' => 'login');
				$this->session->set_userdata($session);
				redirect(base_url().'admin');
			}
			else{
				$this->session->set_flashdata('alert', 'Login gagal! Username atau password salah.');
				redirect(base_url());
			}
		}
		else{
			$this->session->set_flashdata('alert', 'Anda Belum mengisi Username atau Password');
			$this->load->view('login');
		}
	}
}
