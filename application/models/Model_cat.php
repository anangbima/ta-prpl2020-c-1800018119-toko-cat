<?php 
defined('BASEPATH') or exit ('No Direct Script Access Allowed');

/**
 * 
 */
class Model_cat extends CI_Model
{
	
	function edit_data($where,$table){
		return $this->db->get_where($table,$where);
	}

	function get_data($table){
		return $this->db->get($table);
	}

	function insert_data($data,$table){
		$this->db->insert($table,$data);
	}

	function update_data($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}

	function delete_data($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
	}

	function cari($table, $where, $min, $max){
		$this->db->select('*');
		$this->db->from('produk');
		$this->db->like('merk',$where, 'AND');
		//$this->db->or_like('warna', $color, 'AND');
		//$this->db->or_like('harga',$where);
		//$this->db->or_like('id_produk',$where);
		$this->db->where('harga >= ',$min);
		$this->db->where('harga <= ',$max);


		return $this->db->get()->result();

		//return $this->db->query("SELECT * FROM '$table' where merk like '%$where%' or satuan like '%$where%' or harga BETWEEN $min' AND '$max'");
	}

	function cari_produk($table, $where){
		$this->db->select('*'); 
		$this->db->from('produk');
		$this->db->like('merk',$where);
		$this->db->or_like('satuan',$where);
		$this->db->or_like('harga',$where);
		$this->db->or_like('id_produk',$where);
		return $this->db->get()->result();
	}

	function cari_pembeli($tabel, $where){
		$this->db->select('*');
		$this->db->from('pembeli');
		$this->db->like('id_pembeli',$where);
		$this->db->or_like('nama_pembeli',$where);
		$this->db->or_like('jenis_kelamin',$where);
		$this->db->or_like('no_telepon',$where);
		$this->db->or_like('alamat',$where);
		return $this->db->get()->result();
	}

	function cari_transaksi($tabel, $where){
		$this->db->select('*');
		$this->db->from('transaksi');
		$this->db->like('id_transaksi',$where);
		$this->db->or_like('id_pembeli',$where);
		$this->db->or_like('id_produk',$where);
		$this->db->or_like('tgl_transaksi',$where);
		$this->db->or_like('total_pembayaran',$where);
		$this->db->or_like('jumlah_beli',$where);
		$this->db->or_like('status',$where);
		return $this->db->get()->result();
	}

	function kode_produk(){
		$query = $this->db->query("SELECT MAX(id_produk) as kodebaru from produk");
		$hasil = $query->row();
		return $hasil->kodebaru;
	}

	function kode_pembeli(){
		$query = $this->db->query("SELECT MAX(id_pembeli) as kodebaru from pembeli");
		$hasil = $query->row();
		return $hasil->kodebaru;
	}

	function kode_transaksi(){
		$query = $this->db->query("SELECT MAX(id_transaksi) as kodebaru from transaksi");
		$hasil = $query->row();
		return $hasil->kodebaru;
	}

	function populer(){
		$query1 = $this->db->query("SELECT * FROM produk as pro");
		$query2 = $this->db->query("SELECT * FROM produk as prok order by id_produk DESC");

		$result1 = $query1->result();
		$result2 = $query2->result();	
		
		return array_merge($result1, $result2);	
	}
}

?>